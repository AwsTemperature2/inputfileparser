﻿using Amazon.S3;
using AutoMapper;
using InputFileParser.Contracts;
using InputFileParser.Domain;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Temperature.InputFileParser.Contracts;
using Temperature.InputFileParser.Infrastructure.Options;
using Temperature.InputFileParser.Models;

namespace Temperature.InputFileParser.Services
{
    public class InputFileParser : IInputFileParser
    {
        private readonly IAmazonS3 s3Client;
        private readonly ILambdaCaller lambdaCaller;
        private readonly IMapper mapper;
        private readonly ITemperatureFileParser parser;
        private readonly ServiceOptions lambdaOptions;
        private readonly ILogger logger;

        public InputFileParser(IAmazonS3 s3Client,
                               ILambdaCaller lambdaCaller,
                               ITemperatureFileParser parser,
                               IOptions<ServiceOptions> serviceOptions,
                               IMapper mapper,
                               ILogger<InputFileParser> logger
            )
        {
            this.s3Client = s3Client ?? throw new ArgumentNullException(nameof(s3Client));
            this.lambdaCaller = lambdaCaller ?? throw new ArgumentNullException(nameof(lambdaCaller));
            this.parser = parser ?? throw new ArgumentNullException(nameof(parser));
            this.lambdaOptions = serviceOptions.Value ?? throw new ArgumentNullException(nameof(serviceOptions));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<InputFileData> ParseInputFile(string bucket, string key)
        {
            List<TemperaturePoint> fileData;
            long bytes = 0;
            var start = DateTime.Now;

            using (var fileStream = await this.s3Client.GetObjectStreamAsync(bucket, key, null)) // todo using unly for necessary stuff
            {
                fileData = await parser.ParseInputJsonFile(fileStream);
                bytes = fileStream.Length;
            }

            var dayData = fileData.GroupBy(p => p.ts.Date)
                                .Select(g => new TemperatureDay
                                {
                                    ts = g.Key.ToString("yyyy-MM-dd"), // dehardcodize format
                                    data = g.ToList()
                                }
                                )
                                .ToList();

            logger.LogInformation($"Parsed data for {dayData.Count()} days, total {fileData.Count()} temperature points");

            await SaveTemperatureData(dayData);

            return new InputFileData // todo no new
            {
                Bucket = bucket,
                Key = key,
                Dates = dayData.Select(d => d.ts).ToList(),
                Bytes = bytes,
                ProcessingStarted = start,
                Records = fileData.Count,
                FirstRecordTimestamp = fileData.Min(f => f.ts),
                LastRecordTimestamp = fileData.Max(f => f.ts),

            };
        }

        public async Task SaveTemperatureData(List<TemperatureDay> dayData)
        {
            logger.LogInformation($"Calling save data");
            var request = mapper.Map<SaveTemperatureDataRequest>(dayData);
            await lambdaCaller.CallLambda(lambdaOptions, request);
        }
    }
}
