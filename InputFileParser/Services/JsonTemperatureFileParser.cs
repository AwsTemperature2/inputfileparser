﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Temperature.InputFileParser.Contracts;
using Temperature.InputFileParser.Models;

namespace Temperature.InputFileParser.Services
{
    public class JsonTemperatureFileParser : ITemperatureFileParser
    {
        public JsonTemperatureFileParser()
        {

        }

        public async Task<List<TemperaturePoint>> ParseInputJsonFile(Stream stream)
        {
            var settings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var result = new List<TemperaturePoint>();

            using (var reader = new StreamReader(stream))
            {
                while (!reader.EndOfStream)
                {
                    var line = await reader.ReadLineAsync();

                    try
                    {
                        var point = JsonConvert.DeserializeObject<TemperaturePoint>(line, settings);
                        result.Add(point);
                    }
                    catch (Exception ex)
                    {
                        // TODO log which line failed AND/OR count number of errors and return it
                    }
                }
            }

            return result;
        }
    }
}
