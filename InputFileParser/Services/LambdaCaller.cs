﻿using Amazon.Lambda;
using Amazon.Lambda.Model;
using InputFileParser.Contracts;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using Temperature.InputFileParser.Infrastructure.Options;

namespace InputFileParser.Services
{
    public class LambdaCaller : ILambdaCaller
    {
        private readonly IAmazonLambda client;

        public LambdaCaller(IAmazonLambda client)
        {
            this.client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<string> CallLambda(ServiceOptions lambdaOptions, object payload)
        {
            var invokeRequest = new InvokeRequest
            {
                FunctionName = lambdaOptions.SaveLambdaName,
                InvocationType = InvocationType.RequestResponse,
                Payload = JsonConvert.SerializeObject(payload)
            };

            var response = await client.InvokeAsync(invokeRequest);
            using (var sr = new StreamReader(response.Payload))
            {
                var content = await sr.ReadToEndAsync();
                return content;
            }
        }

        public async Task<TResponse> GetLambdaResponse<TResponse>(ServiceOptions lambdaOptions, object payload)
        {
            var resultString = await CallLambda(lambdaOptions, payload);

            return JsonConvert.DeserializeObject<TResponse>(resultString);
        }
    }
}
