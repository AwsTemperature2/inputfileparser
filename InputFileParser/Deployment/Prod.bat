cd..
dotnet restore
dotnet lambda deploy-serverless input-file-parser-prod --region eu-central-1 --profile default -tp "EnvironmentName=Production"
cd Deployment
pause
