using System;
using System.Threading.Tasks;
using Amazon.Lambda;
using Amazon.Lambda.Core;
using Amazon.S3;
using InputFileParser.Contracts;
using InputFileParser.Domain;
using InputFileParser.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Temperature.InputFileParser.Contracts;
using Temperature.InputFileParser.Domain;
using Temperature.InputFileParser.Infrastructure.Options;
using Temperature.InputFileParser.Services;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Temperature.InputFileParser
{
    public class ParserFunction : LambdaBase
    {
        private readonly ServiceOptions options;

        public ParserFunction()
        {
            this.options = GetService<IOptions<ServiceOptions>>().Value;
        }

        protected override void ConfigureServices(IServiceCollection services)
        {
            services.AddAWSService<IAmazonLambda>();
            services.AddAWSService<IAmazonS3>();

            services.AddTransient<IInputFileParser, Services.InputFileParser>();
            services.AddTransient<ITemperatureFileParser, JsonTemperatureFileParser>();
            services.AddTransient<ILambdaCaller, LambdaCaller>();
        }

        protected override void InjectOptions(IServiceCollection services)
        {
            services.Configure<ServiceOptions>(Configuration.GetSection("ServiceOptions"));
        }

        public async Task<InputFileData> ParseInputFile(ParseInputFileRequest request, ILambdaContext context)
        {
            context?.Logger?.LogLine($"Parsing file {request.Key}");

            var service = this.GetService<IInputFileParser>();

            return await service.ParseInputFile(options.SourceBucket,request.Key);
        }

    }
}
