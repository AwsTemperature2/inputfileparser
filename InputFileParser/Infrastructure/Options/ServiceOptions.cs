﻿namespace Temperature.InputFileParser.Infrastructure.Options
{
    public class ServiceOptions
    {
        public string SaveLambdaName { get; set; }
        public string SourceBucket { get; set; }
    }
}
