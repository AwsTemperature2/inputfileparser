﻿using AutoMapper;
using InputFileParser.Domain;
using System.Collections.Generic;
using Temperature.InputFileParser.Models;

namespace Temperature.InputFileParser.Infrastructure.Mapping
{
    public class DefaultProfile : Profile
    {
        public DefaultProfile()
        {
            this.CreateMap<List<TemperatureDay>, SaveTemperatureDataRequest>()
               .ForMember(d => d.Interval, o => o.UseValue(1))
               .ForMember(d => d.Data, o => o.MapFrom(s => s));
        }
    }
}
