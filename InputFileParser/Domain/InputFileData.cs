﻿using System;
using System.Collections.Generic;

namespace InputFileParser.Domain
{
    public class InputFileData
    {
        public string Bucket { get; set; }
        public string Key { get; set; }
        public long Bytes { get; set; }
        public int Records { get; set; }
        public int BadRecords { get; set; }
        public DateTime ProcessingStarted { get; set; }
        public DateTime FirstRecordTimestamp { get; set; }
        public DateTime LastRecordTimestamp { get; set; }
        public List<string> Dates { get; set; }
    }
}
