﻿using System.Collections.Generic;
using Temperature.InputFileParser.Models;

namespace InputFileParser.Domain
{
    public class SaveTemperatureDataRequest
    {
        public int Interval { get; set; }
        public IEnumerable<TemperatureDay> Data { get; set; }
    }
}
