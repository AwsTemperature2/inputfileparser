﻿using System.Threading.Tasks;
using Temperature.InputFileParser.Infrastructure.Options;

namespace InputFileParser.Contracts
{
    public interface ILambdaCaller
    {
        Task<string> CallLambda(ServiceOptions lambdaOptions, object payload);
        Task<TResponse> GetLambdaResponse<TResponse>(ServiceOptions lambdaOptions, object payload);
    }
}
