﻿using InputFileParser.Domain;
using System.Threading.Tasks;

namespace Temperature.InputFileParser.Contracts
{
    public interface IInputFileParser
    {
        Task<InputFileData> ParseInputFile(string bucket, string key);
    }
}
