﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Temperature.InputFileParser.Models;

namespace Temperature.InputFileParser.Contracts
{
    public interface ITemperatureFileParser
    {
       Task<List<TemperaturePoint>> ParseInputJsonFile(Stream stream);
    }
}
