﻿using System.Collections.Generic;

namespace Temperature.InputFileParser.Models
{
    
    public class TemperatureDay
    {
    
        public string ts { get; set; }

        public List<TemperaturePoint> data { get; set; }
    }
}
