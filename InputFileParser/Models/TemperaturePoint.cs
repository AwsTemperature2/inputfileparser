﻿using System;

namespace Temperature.InputFileParser.Models
{
    // TODO properly solve case of properties
    
    public class TemperaturePoint
    {
    
        public DateTime ts { get; set; }

    
        public double? r { get; set; }

    
        public double? h { get; set; }

    
        public double? o { get; set; }
    }
}
