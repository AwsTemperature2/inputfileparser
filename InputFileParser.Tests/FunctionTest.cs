using System;
using System.Threading.Tasks;
using Xunit;
using Temperature.InputFileParser;
using Temperature.InputFileParser.Domain;
using Amazon.Lambda.TestUtilities;

namespace InputFileParser.Tests
{
    public class FunctionTest
    {
        public FunctionTest()
        {
            Environment.SetEnvironmentVariable(LambdaBase.EnvironmentVariable, "Development");
        }

        [Fact]
        public async Task TestProcessFile()
        {
            var key = "dev/jsoncmd-2018-09-01_00-00-01";

            var request = new ParseInputFileRequest
            {
                Key = key
            };

            var context = new TestLambdaContext();

            var function = new ParserFunction();
            var result = await function.ParseInputFile(request , context);
        }
    }
}
